/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class STPConsumerApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(STPConsumerApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(STPConsumerApplication.class, args);
	}
	
}
