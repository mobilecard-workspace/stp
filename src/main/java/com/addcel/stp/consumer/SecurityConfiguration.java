/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.addcel.stp.consumer.constants.MobileCardConstants;
import com.addcel.stp.consumer.domain.AppAuthorization;
import com.addcel.stp.consumer.repository.AppAuthorizationRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AppAuthorizationRepository appAuthRepo;
	
	private static final Logger LOGGER = LogManager.getLogger(SecurityConfiguration.class);
	
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		LOGGER.debug("Consultando usuarios autorizados en la BD ...");
		List<AppAuthorization> appAuthList = appAuthRepo.findByActivo(MobileCardConstants.USUARIO_WS_ACTIVO);
		
		for(AppAuthorization appAuth : appAuthList) {
			auth.inMemoryAuthentication().withUser(appAuth.getUsername()).password(appAuth.getPassword()).roles("ADMIN");
			LOGGER.debug("Usuario agregado para consumir la API REST: " + appAuth.getUsername());
		}
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/api/*").authenticated().and().httpBasic()
				.authenticationEntryPoint(getBasicAuthEntryPoint()).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
		return new CustomBasicAuthenticationEntryPoint();
	}

	/* To allow Pre-flight [OPTIONS] request from browser */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}

}
