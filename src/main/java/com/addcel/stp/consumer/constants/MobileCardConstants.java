/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.constants;

public class MobileCardConstants {
	
	public static final int USUARIO_ACTIVO = 1;
	public static final int USUARIO_ACTIVO_2 = 99;
	public static final int USUARIO_BLOQUEADO = 3;
	public static final int USUARIO_RESET = 98;
	public static final char USUARIO_WS_ACTIVO = 'T';
	
}
