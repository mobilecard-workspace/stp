/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.constants;

public class STPConstants {

	public static final String REGISTRA_ORDEN_URL = "registra";
	public static final String INSTITUCION_OPERANTE = "90646";
	public static final String CURP_BENEFICIARIO_DEFAULT = "ND";
	public static final String TIPO_PAGO_DEFAULT = "1";
	public static final String STP_DATE_FORMAT = "yyyyMMdd";
	
}
