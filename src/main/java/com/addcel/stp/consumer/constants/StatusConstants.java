/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.constants;

public class StatusConstants {

	//Codigos de error
	public static final int SUCCESS_CODE = 1000;
	public static final int ERROR_CODE = 2000;
	public static final int STP_WS_UNAVAILABLE_CODE = 2100;
	public static final int INTERNAL_SERVER_ERROR_CODE = 2200;
	public static final int USER_BLOCKED_CODE = 2400;
	public static final int HOUR_NOT_PERMITED_CODE = 2500;
	public static final int CONCEPT_LENGTH_CODE = 2600;
	public static final int PERSIST_TRANSACTION_ERROR_CODE = 2700;
	
	//Mensajes de error
	public static final String ENVIAR_PAGO_SUCCESS = "Se ha enviado correctamente el dinero";
	public static final String USER_BLOCKED_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + USER_BLOCKED_CODE;
	public static final String STP_WS_UNAVAILABLE_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + STP_WS_UNAVAILABLE_CODE;
	public static final String INTERNAL_SERVER_ERROR_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + INTERNAL_SERVER_ERROR_CODE;
	public static final String PERSIST_TRANSACTION_ERROR_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + PERSIST_TRANSACTION_ERROR_CODE;
	public static final String HOUR_NOT_PERMITED_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + HOUR_NOT_PERMITED_CODE;
	public static final String CONCEPT_LENGTH_MSG = "Su solicitud no pudo ser procesada, int�ntelo nuevamente. C�digo de error: SPC" + CONCEPT_LENGTH_CODE;
	
}
