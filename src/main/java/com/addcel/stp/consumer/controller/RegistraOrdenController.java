/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.controller;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.stp.consumer.constants.StatusConstants;
import com.addcel.stp.consumer.request.EnviarPagoRequest;
import com.addcel.stp.consumer.response.ApiResponse;
import com.addcel.stp.consumer.service.RegistraOrdenService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
@Validated
public class RegistraOrdenController {
	
	private static final Logger LOGGER = LogManager.getLogger(RegistraOrdenController.class);
	
	@Autowired
	private RegistraOrdenService registraOrdenServ;
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/enviarDinero", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object agregarDinero(@Valid @RequestBody EnviarPagoRequest enviarPagoReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.debug("EnviarPagoReq: " + enviarPagoReq.toString());
		
		if(enviarPagoReq.getIdUsuario() != null) {
			LOGGER.info("Realizando envio de pago para el usuario: " + enviarPagoReq.getIdUsuario());
		} else if(enviarPagoReq.getIdEstablecimiento() !=  null) {
			LOGGER.info("Realizando envio de pago para el establecimiento: " + enviarPagoReq.getIdEstablecimiento());
		}
		
		resp = registraOrdenServ.registrarOrden(enviarPagoReq, false);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha enviado correctamente el dinero, se espera la confirmacion por parte de STP");
		} else {
			if(enviarPagoReq.getIdUsuario() != null) {
				LOGGER.warn("No se pudo enviar el dinero para el usuario: " + enviarPagoReq.getIdUsuario());
			} else if(enviarPagoReq.getIdEstablecimiento() !=  null) {
				LOGGER.warn("No se pudo enviar el dinero para el establecimiento: " + enviarPagoReq.getIdEstablecimiento());
			}
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/enviarConciliacion", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object enviarConciliacion(@Valid @RequestBody EnviarPagoRequest enviarPagoReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.debug("EnviarPagoReq: " + enviarPagoReq.toString());
		
		LOGGER.info("Realizando envio de pago de conciliacion para: " + enviarPagoReq.getNombreBeneficiario());
		
		resp = registraOrdenServ.registrarOrden(enviarPagoReq, true);
				
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha enviado correctamente el dinero de la conciliacion, se espera la confirmacion por parte de STP");
		} else {
			LOGGER.warn("No se pudo enviar el dinero de la conciliacion para:" + enviarPagoReq.getNombreBeneficiario());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
		LOGGER.error("Uno o mas parametros no fueron recibidos correctamente");
		return new ResponseEntity<>("Error en los parametros: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	}

}
