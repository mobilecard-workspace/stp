/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "APP_AUTHORIZATION")
public class AppAuthorization {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_app_auth;
	
	@Column(name = "id_application", nullable = false)
	private String idApplication;
		
	@Column(name = "username", nullable = false)
	private String username;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "activo", nullable = false)
	private char activo;
	
}
