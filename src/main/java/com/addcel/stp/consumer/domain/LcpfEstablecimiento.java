/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "LCPF_establecimiento")
public class LcpfEstablecimiento {
	
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "usuario", nullable = false)
	private String usuario;
	
	@Column(name = "nombre_establecimiento", nullable = true)
	private String nombreEstablecimiento;
	
	@Column(name = "rfc", nullable = true)
	private String rfc;
	
	@Column(name = "razon_social", nullable = true)
	private String razonSocial;
	
	@Column(name = "estatus", nullable = true)
	private int estatus;
	
	@Column(name = "representante_nombre", nullable = true)
	private String representanteNombre;
	
	@Column(name = "representante_paterno", nullable = true)
	private String representantePaterno;
	
	@Column(name = "representante_materno", nullable = true)
	private String representanteMaterno;
	
	@Column(name = "representante_curp", nullable = true)
	private String representanteCurp;
	
	@Column(name = "calle", nullable = true)
	private String calle;
	
	@Column(name = "colonia", nullable = true)
	private String colonia;
	
	@Column(name = "cp", nullable = true)
	private String cp;
	
	@Column(name = "municipio", nullable = true)
	private String municipio;
	
	@Column(name = "id_estado", nullable = true)
	private Long idEstado;
	
}
