/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stp_transacciones_dispersion")
public class StpTransaccionesDispersion {
	
	@Id
	@Column(name = "id_stp_transacciones_dispersion", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idStpTransaccionesDispersion;
	
	@Column(name = "folio", nullable = false)
	private String folio;
	
	@Column(name = "referencia", nullable = false)
	private String referencia;
	
	@Column(name = "clave_rastreo", nullable = false)
	private String claveRastreo;
	
	@Column(name = "id_orden", nullable = false)
	private Long idOrden;
	
	@Column(name = "monto", nullable = false)
	private Double monto;
	
	@Column(name = "concepto_pago", nullable = false)
	private String conceptoPago;
	
	@Column(name = "cuenta_beneficiario", nullable = false)
	private String cuentaBeneficiario;
	
	@Column(name = "institucion_contraparte", nullable = false)
	private String institucionContraparte;
	
	@Column(name = "nombre_beneficiario", nullable = false)
	private String nombreBeneficiario;
	
	@Column(name = "tipo_cuenta_beneficiario", nullable = false)
	private String tipoCuentaBeneficiario;
	
	@Column(name = "id_devolucion", nullable = true)
	private Integer idDevolucion;
	
	@Column(name = "fecha", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Column(name = "id_usuario", nullable = true)
	private Long idUsuario;
	
	@Column(name = "id_establecimiento", nullable = true)
	private Long idEstablecimiento;

}
