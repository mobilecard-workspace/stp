/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_usuarios")
public class TUsuarios {

	@Id
	@Column(name = "id_usuario", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idUsuario;
	
	@Column(name = "usr_login", nullable = false)
	private String usrLogin;
	
	@Column(name = "usr_nombre", nullable = true)
	private String usrNombre;
	
	@Column(name = "usr_apellido", nullable = true)
	private String usrApellido;
	
	@Column(name = "id_usr_status", nullable = false)
	private Integer idUsrStatus;
	
	@Column(name = "usr_materno", nullable = true)
	private String usrMaterno;
	
	@Column(name = "usr_rfc", nullable = true)
	private String usrRfc;
	
	@Column(name = "usr_curp", nullable = true)
	private String usrCurp;
	
	@Column(name = "usr_direccion", nullable = true)
	private String usrDireccion;
	
	@Column(name = "usr_colonia", nullable = true)
	private String usrColonia;
	
	@Column(name = "usr_cp", nullable = true)
	private String usrCp;
	
	@Column(name = "usr_ciudad", nullable = true)
	private String usrCiudad;
	
	@Column(name = "usr_id_estado", nullable = true)
	private Integer usrIdEstado;
	
}
