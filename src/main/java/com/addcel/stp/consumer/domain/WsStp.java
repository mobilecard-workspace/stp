/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ws_stp")
public class WsStp {
	
	@Id
	@Column(name = "id_ws_stp", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idWsStp;
	
	@Column(name = "peticion", nullable = false)
	private String peticion;
	
	@Column(name = "respuesta", nullable = false)
	private String respuesta;
	
	@Column(name = "id_orden", nullable = false)
	private Long idOrden;
	
	@Column(name = "fecha", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Column(name = "id_usuario", nullable = true)
	private Long idUsuario;
	
	@Column(name = "id_establecimiento", nullable = true)
	private Long idEstablecimiento;

}
