/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.consumer.domain.AppAuthorization;

public interface AppAuthorizationRepository extends CrudRepository<AppAuthorization, Long> {

	public AppAuthorization findByUsernameAndActivo(@Param("username") String username,
			@Param("activo") char activo);
	
	public List<AppAuthorization> findByActivo(@Param("activo") char activo);
	
}
