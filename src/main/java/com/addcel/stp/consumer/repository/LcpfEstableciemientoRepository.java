/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.consumer.domain.LcpfEstablecimiento;

public interface LcpfEstableciemientoRepository extends CrudRepository<LcpfEstablecimiento, Long> {

	public LcpfEstablecimiento findByIdAndEstatus(@Param("id") long id, @Param("estaus") int estaus);
	
}
