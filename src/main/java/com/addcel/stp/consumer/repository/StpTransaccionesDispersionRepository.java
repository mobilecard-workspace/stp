/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.consumer.domain.StpTransaccionesDispersion;

public interface StpTransaccionesDispersionRepository extends CrudRepository<StpTransaccionesDispersion, Long> {

	public StpTransaccionesDispersion findByIdOrden(@Param("id_orden") int id_orden);
	
	@SuppressWarnings("unchecked")
	public StpTransaccionesDispersion save(StpTransaccionesDispersion stpTransaccionesDispersion);
	
}
