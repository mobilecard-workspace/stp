/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.consumer.domain.TUsuarios;

public interface TUsuariosRepository extends CrudRepository<TUsuarios, Long> {
	
	public TUsuarios findByIdUsuarioAndIdUsrStatus(@Param("idUsuario") long idUsuario, @Param("idUsrStatus") int status);

}
