/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.stp.consumer.domain.WsStp;

public interface WsStpRepository extends CrudRepository<WsStp, Long> {

	@SuppressWarnings("unchecked")
	public WsStp save(WsStp wsStp);
	
}
