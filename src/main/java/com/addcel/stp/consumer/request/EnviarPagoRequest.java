/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class EnviarPagoRequest {
	
	private Long idUsuario;
	
	private Long idEstablecimiento;
	
	@NotEmpty
	private String conceptoPago;
	
	@NotEmpty
	private String cuentaBeneficiario;
	
	@NotEmpty
	private String institucionContraparte;
	
	@NotNull
	private String monto;
	
	@NotEmpty
	private String nombreBeneficiario;
	
	@NotEmpty
	private String tipoCuentaBeneficiario;
	
}
