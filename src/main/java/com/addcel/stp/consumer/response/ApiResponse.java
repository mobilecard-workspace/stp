/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private Integer code;
	private String message;
	private Long idOperacion;
	private String claveRastreo;
	private String folio;
	private String referencia;
	private Long idStpTransaccionesDispersion;
	private Long idPeticionWsStp;
	
}
