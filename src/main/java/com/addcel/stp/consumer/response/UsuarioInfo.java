/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioInfo {

	private int status;
	private String fullname;
	
}
