/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.service;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.stp.consumer.constants.MobileCardConstants;
import com.addcel.stp.consumer.constants.STPConstants;
import com.addcel.stp.consumer.constants.StatusConstants;
import com.addcel.stp.consumer.domain.StpTransaccionesDispersion;
import com.addcel.stp.consumer.repository.StpTransaccionesDispersionRepository;
import com.addcel.stp.consumer.request.EnviarPagoRequest;
import com.addcel.stp.consumer.response.ApiResponse;
import com.addcel.stp.consumer.response.UsuarioInfo;
import com.addcel.stp.consumer.util.FieldsValidator;
import com.addcel.stp.consumer.util.Foliador;
import com.addcel.stp.consumer.util.GeneradorFirma;
import com.addcel.stp.consumer.util.PersistRequest;
import com.addcel.stp.consumer.util.PropertiesFile;
import com.addcel.stp.consumer.util.RestClient;
import com.addcel.stp.consumer.util.ValidarUsuarios;
import com.addcel.stp.consumer.ws.request.RegistraOrdenRequest;
import com.addcel.stp.consumer.ws.response.BaseResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class RegistraOrdenService {
	
	private static final Logger LOGGER = LogManager.getLogger(RegistraOrdenService.class);
	
	@Autowired
	private ValidarUsuarios validarUsuarios;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private Foliador foliador;
	
	@Autowired
	private GeneradorFirma generadorFirma;
	
	@Autowired
	private StpTransaccionesDispersionRepository stpTransaccionesRepo;
	
	@Autowired
	private PersistRequest persistReq;
	
	@Autowired
	private FieldsValidator fieldsValidator;
	
	public ApiResponse registrarOrden(EnviarPagoRequest enviarPagoReq, boolean conciliacion) {
		ApiResponse apiResp = new ApiResponse();
		UsuarioInfo usuarioInfo = null;
		int status = MobileCardConstants.USUARIO_BLOQUEADO;
		Long idUsuario = 0L;
		Long idEstablecimiento = 0L;
		
		//Si no es un envio de dinero de conciliacion
		if(!conciliacion) {
			idUsuario = enviarPagoReq.getIdUsuario();
			idEstablecimiento = enviarPagoReq.getIdEstablecimiento();
			
			usuarioInfo = validarUsuarios.getUserInfo(idUsuario, idEstablecimiento);
			status = usuarioInfo.getStatus();
		}
		
		if(status == MobileCardConstants.USUARIO_ACTIVO || conciliacion) {
			LOGGER.info("Usuario encontrado y activo! se procede con el envio del dinero");
			
			String fullname = conciliacion ? propsFile.getStpDefaultNombreBeneficiario() : 
				fieldsValidator.eliminarAcentos(usuarioInfo.getFullname().trim());
			String concepto = fieldsValidator.eliminarAcentos(enviarPagoReq.getConceptoPago().trim());
			String beneficiario = fieldsValidator.eliminarAcentos(enviarPagoReq.getNombreBeneficiario().trim());
			
			long idOrdenSTP = 0;
			
			//String fechaOperacion = new SimpleDateFormat(STPConstants.STP_DATE_FORMAT).format(fecha);
			String clave = foliador.generarClave();
			String folio = foliador.generarFolio();
			String referencia = foliador.generarReferencia();
			String monto = enviarPagoReq.getMonto().replace(",", ".");
			concepto = concepto.length() > propsFile.getStpConceptoLength() ? concepto.substring(0, 40) : concepto;
			String firmaElectronica = generadorFirma.generarFirmaElectronica(enviarPagoReq.getInstitucionContraparte(), 
					propsFile.getStpWsRestEmpresa(), folio, clave, monto, 
					fullname, enviarPagoReq.getTipoCuentaBeneficiario(), beneficiario, 
					enviarPagoReq.getCuentaBeneficiario(), concepto, referencia);
			
			RegistraOrdenRequest regOrdenReq = new RegistraOrdenRequest();
			
			//Obligatorios
			regOrdenReq.setClaveRastreo(clave);
			regOrdenReq.setConceptoPago(concepto);
			regOrdenReq.setCuentaBeneficiario(enviarPagoReq.getCuentaBeneficiario());
			regOrdenReq.setEmpresa(propsFile.getStpWsRestEmpresa());
			regOrdenReq.setFirma(firmaElectronica);
			regOrdenReq.setInstitucionContraparte(enviarPagoReq.getInstitucionContraparte());
			regOrdenReq.setInstitucionOperante(STPConstants.INSTITUCION_OPERANTE);
			regOrdenReq.setMonto(monto);
			regOrdenReq.setNombreBeneficiario(beneficiario);
			regOrdenReq.setReferenciaNumerica(referencia);
			regOrdenReq.setRfcCurpBeneficiario(STPConstants.CURP_BENEFICIARIO_DEFAULT);
			regOrdenReq.setTipoCuentaBeneficiario(enviarPagoReq.getTipoCuentaBeneficiario());
			regOrdenReq.setTipoPago(STPConstants.TIPO_PAGO_DEFAULT);
			
			//No Obligatorios
			regOrdenReq.setNombreOrdenante(fullname);
			regOrdenReq.setCuentaOrdenante(propsFile.getStpCuentaOrdenante());
			regOrdenReq.setTipoCuentaOrdenante(propsFile.getStpTipoCuentaOrdenante());
			regOrdenReq.setRfcCurpOrdenante(propsFile.getStpRfcOrdenante());
			regOrdenReq.setFolioOrigen(folio);
			//regOrdenReq.setFechaOperacion(fechaOperacion);
			
			BaseResponse response = null;
			
			try {
				response = restClient.createRequest(STPConstants.REGISTRA_ORDEN_URL, regOrdenReq);
			} catch (JsonProcessingException ex) {
				LOGGER.error("Error de parseo de JSON: " + ex.getMessage());
				ex.printStackTrace();
				
				return new ApiResponse(StatusConstants.INTERNAL_SERVER_ERROR_CODE, StatusConstants.INTERNAL_SERVER_ERROR_MSG, null, null, null, null, null, null);
			} catch (Exception ex) {
				LOGGER.error("Error al consumir el WS REST del STP: " + ex.getMessage());
				ex.printStackTrace();
				
				return new ApiResponse(StatusConstants.STP_WS_UNAVAILABLE_CODE, StatusConstants.STP_WS_UNAVAILABLE_MSG, null, null, null, null, null, null);
			}
			
			if(response.getResultado().getId() <= 0) {
				LOGGER.warn("No se pudo enviar el dinero: " + response.getResultado().getDescripcionError());
				
				apiResp.setCode(StatusConstants.ERROR_CODE);
				apiResp.setMessage(response.getResultado().getDescripcionError());
			} else {
				idOrdenSTP = response.getResultado().getId();
				LOGGER.info("El dinero se envio exitosamente, ID de la Orden: " + idOrdenSTP);
				
				StpTransaccionesDispersion stpTransaccion = new StpTransaccionesDispersion();
				stpTransaccion.setClaveRastreo(clave);
				stpTransaccion.setFecha(new Date());
				stpTransaccion.setFolio(folio);
				stpTransaccion.setIdDevolucion(null);
				stpTransaccion.setIdOrden(idOrdenSTP);
				stpTransaccion.setMonto(Double.valueOf(monto));
				stpTransaccion.setConceptoPago(enviarPagoReq.getConceptoPago());
				stpTransaccion.setCuentaBeneficiario(enviarPagoReq.getCuentaBeneficiario());
				stpTransaccion.setInstitucionContraparte(enviarPagoReq.getInstitucionContraparte());
				stpTransaccion.setNombreBeneficiario(enviarPagoReq.getNombreBeneficiario());
				stpTransaccion.setTipoCuentaBeneficiario(enviarPagoReq.getTipoCuentaBeneficiario());
				stpTransaccion.setIdEstablecimiento(idEstablecimiento);
				stpTransaccion.setIdUsuario(idUsuario);
				stpTransaccion.setReferencia(referencia);
				
				LOGGER.debug("Guardando la transaccion de dispersion en la BD ...");
				
				try {
					StpTransaccionesDispersion stpTransaccionSaved = stpTransaccionesRepo.save(stpTransaccion);
					LOGGER.info("Se ha persistido correctamente la transaccion de STP en la BD: " + stpTransaccionSaved.toString());
					
					apiResp.setCode(StatusConstants.SUCCESS_CODE);
					apiResp.setMessage(StatusConstants.ENVIAR_PAGO_SUCCESS);
					apiResp.setIdOperacion(idOrdenSTP);
					apiResp.setClaveRastreo(clave);
					apiResp.setFolio(folio);
					apiResp.setReferencia(referencia);
					apiResp.setIdStpTransaccionesDispersion(stpTransaccionSaved.getIdStpTransaccionesDispersion());
				} catch(Exception ex) {
					LOGGER.error("No se pudo persistir en la BD la transaccion de STP: " + ex.getMessage());
					LOGGER.warn("Persistir manualmente en la BD el idOrden: " + idOrdenSTP);
					ex.printStackTrace();
					
					apiResp.setCode(StatusConstants.SUCCESS_CODE);
					apiResp.setMessage(StatusConstants.PERSIST_TRANSACTION_ERROR_MSG);
					apiResp.setIdOperacion(idOrdenSTP);
					apiResp.setClaveRastreo(clave);
					apiResp.setFolio(folio);
					apiResp.setReferencia(referencia);
					apiResp.setIdStpTransaccionesDispersion(null);
				}
			}
			
			long idPeticionWsStp = persistReq.saveRequest(idOrdenSTP, regOrdenReq, response, idUsuario, idEstablecimiento);
			apiResp.setIdPeticionWsStp(idPeticionWsStp);
		} else {
			LOGGER.error("El usuario esta bloqueado, no se podra enviar el dinero");
			return new ApiResponse(StatusConstants.USER_BLOCKED_CODE, StatusConstants.USER_BLOCKED_MSG, null, null, null, null, null, null);
		}
		
		return apiResp;
	}
	
}
