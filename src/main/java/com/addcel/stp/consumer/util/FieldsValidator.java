/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class FieldsValidator {
		
	private static final Logger LOGGER = LogManager.getLogger(FieldsValidator.class);
	
	public String eliminarAcentos(String texto) {
		String sinAcentos = "";
		
		if(texto == null) {
			return sinAcentos;
		}
		
		sinAcentos = StringUtils.stripAccents(texto);
		LOGGER.debug("Texto sin acentos: " + sinAcentos);
		
		return sinAcentos;
	}
	
}
