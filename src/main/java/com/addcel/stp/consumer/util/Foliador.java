/**
 * @author Victor Ramirez
 */
package com.addcel.stp.consumer.util;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.uuid.Generators;

@Component
public class Foliador {

	private static final Logger LOGGER = LogManager.getLogger(Foliador.class);
	
	public String generarClave() {
		UUID uuid = Generators.timeBasedGenerator().generate();
		String clave = "ADCEL" + String.valueOf(uuid.timestamp());
		
		LOGGER.debug("Clave generada: " + clave);
		return clave;
	}
	
	public String generarFolio() {
		String folio = UUID.randomUUID().toString();
		
		LOGGER.debug("Folio generado: " + folio);
		return folio;
	}
	
	public String generarReferencia() {
		UUID uuid = Generators.timeBasedGenerator().generate();
		String referencia = String.valueOf(uuid.timestamp());
		referencia = ((int) (Math.random() * 8) + 1) + referencia.substring(referencia.length() - 6, referencia.length());
		
		LOGGER.debug("Referencia generada: " + referencia);
		return referencia;
	}
	
}
