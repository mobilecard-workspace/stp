/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.stp.consumer.constants.STPConstants;

@Component
public class GeneradorFirma {

	private static final Logger LOGGER = LogManager.getLogger(GeneradorFirma.class);
	
	@Autowired
	private RSAUtil rsaUtil;
	
	public String generarFirmaElectronica(String institucionContraparte, String empresa, String folio,
			String clave, String monto, String fullname, String tipoCuenta, String nombreBeneficiario, 
			String cuentaBeneficiario, String conceptoPago, String referencia) {
		
		StringBuilder cadenaOriginalBuilder = new StringBuilder();
		//inicio
		cadenaOriginalBuilder.append("||");
		cadenaOriginalBuilder.append(institucionContraparte);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(empresa);
		cadenaOriginalBuilder.append("|");
		//cadenaOriginalBuilder.append(fechaOperacion);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(folio);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(clave);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(STPConstants.INSTITUCION_OPERANTE);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(monto);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(STPConstants.TIPO_PAGO_DEFAULT);
		cadenaOriginalBuilder.append("||");
		cadenaOriginalBuilder.append(fullname);
		cadenaOriginalBuilder.append("|||");
		cadenaOriginalBuilder.append(tipoCuenta);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(nombreBeneficiario);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(cuentaBeneficiario);
		cadenaOriginalBuilder.append("|");
		cadenaOriginalBuilder.append(STPConstants.CURP_BENEFICIARIO_DEFAULT);
		cadenaOriginalBuilder.append("||||||");
		cadenaOriginalBuilder.append(conceptoPago);
		cadenaOriginalBuilder.append("||||||");
		cadenaOriginalBuilder.append(referencia);
		cadenaOriginalBuilder.append("||||||");
		//termina
		cadenaOriginalBuilder.append("||");
		
		String cadenaOriginal = cadenaOriginalBuilder.toString();
		LOGGER.debug("Cadena Original: " + cadenaOriginal);
		
		String firmaElectronica = rsaUtil.signText(cadenaOriginal);
		LOGGER.debug("Firma Electronica: " + firmaElectronica);
		
		return firmaElectronica;
	}
	
}
