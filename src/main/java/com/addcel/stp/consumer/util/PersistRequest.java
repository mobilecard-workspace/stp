/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.stp.consumer.domain.WsStp;
import com.addcel.stp.consumer.repository.WsStpRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class PersistRequest {

	@Autowired
	private WsStpRepository wsStpRepo;
	
	private static final Logger LOGGER = LogManager.getLogger(PersistRequest.class);
	
	public long saveRequest(Long idOrden, Object peticion, Object respuesta, Long idUsuario, Long idEstablecimiento) {
		LOGGER.debug("Guardando la peticion que se hizo al WS de STP en la BD ...");
		
		ObjectMapper mapper = new ObjectMapper();
		String request = "";
		String response = "";
		
		try {
			request = mapper.writeValueAsString(peticion);
			response = mapper.writeValueAsString(respuesta);
			
			WsStp wsStp = new WsStp();
			wsStp.setIdOrden(idOrden);
			wsStp.setPeticion(request);
			wsStp.setRespuesta(response);
			wsStp.setIdUsuario(idUsuario);
			wsStp.setIdEstablecimiento(idEstablecimiento);
			wsStp.setFecha(new Date());
			
			WsStp wsStpSaved = wsStpRepo.save(wsStp);
			
			if(wsStpSaved != null){
				LOGGER.debug("Peticion realizada al WS de STP guardada correctamente en la BD: " + wsStpSaved.toString());
				return wsStpSaved.getIdWsStp();
			}
			else {
				LOGGER.error("No se pudo guardar la peticion en la BD");
				return 0;
			}
		} catch (JsonProcessingException ex) {
			LOGGER.error("No se pudo parsear a JSON: " + ex.getMessage());
			ex.printStackTrace();
			return 0;
		} catch (Exception ex) {
			LOGGER.error("No se pudo guardar la peticion en la BD: " + ex.getMessage());
			ex.printStackTrace();
			return 0;
		}
	}
	
}
