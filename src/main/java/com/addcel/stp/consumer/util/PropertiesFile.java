/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${stp.ws.rest.url}")
	private String stpWsRestUrl;
	
	@Value("${stp.ws.rest.empresa}")
	private String stpWsRestEmpresa;
	
	@Value("${stp.private.key.file}")
	private String stpPrivateKeyFile;
	
	@Value("${stp.private.key.alias}")
	private String stpPrivateKeyAlias;
	
	@Value("${stp.private.key.password}")
	private String stpPrivateKeyPassword;
	
	@Value("${stp.concepto.length}")
	private int stpConceptoLength;
	
	@Value("${stp.default.nombre.beneficiario}")
	private String stpDefaultNombreBeneficiario;
	
	@Value("${stp.cuenta.ordenante}")
	private String stpCuentaOrdenante;
	
	@Value("${stp.tipo.cuenta.ordenante}")
	private String stpTipoCuentaOrdenante;
	
	@Value("${stp.rfc.ordenante}")
	private String stpRfcOrdenante;
	
}
