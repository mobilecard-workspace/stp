/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RSAUtil {
	
	private static final Logger LOGGER = LogManager.getLogger(RSAUtil.class);

	@Autowired
	private PropertiesFile propsFile;
	
	private RSAPrivateKey getRSAPrivateKey() throws Exception {
        RSAPrivateKey privateKey = null;
        LOGGER.debug("Cargando la llave privada del jks ...");
        
        try {
            KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(new FileInputStream(propsFile.getStpPrivateKeyFile()), propsFile.getStpPrivateKeyPassword().toCharArray());
            privateKey = (RSAPrivateKey) keystore.getKey(propsFile.getStpPrivateKeyAlias(), propsFile.getStpPrivateKeyPassword().toCharArray());
        } catch (Exception ex) {
        	LOGGER.error("Error durante la carga del jks: " + ex.getMessage());
            ex.printStackTrace();
        }
        
        LOGGER.debug("Se ha cargado correctamente la llave privada del jks");
        return privateKey;
    }
	
	public String signText(String cadena) {
        String retVal = "";
        LOGGER.debug("Firmando la cadena original ...");
        
        try {
            String data = cadena;
            Signature firma = Signature.getInstance("SHA256withRSA");
            RSAPrivateKey llavePrivada = this.getRSAPrivateKey();
            firma.initSign(llavePrivada);
            byte[] bytes = data.getBytes("UTF-8");
            firma.update(bytes, 0, bytes.length);
            retVal = Base64.getEncoder().encodeToString(firma.sign());
            
            LOGGER.debug("Se ha firmado correctamente la cadena");
        } catch (Exception e) {
        	LOGGER.error("Ocurrio un error al generar la firma electronica");
			e.printStackTrace();
		}
        
        return retVal;
    }
	
}
