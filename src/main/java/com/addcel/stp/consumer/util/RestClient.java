/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.addcel.stp.consumer.ws.response.BaseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	@Autowired
	private PropertiesFile propsFile;

	public HttpHeaders addHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
		
	public BaseResponse createRequest(String method, Object requestBody) 
			throws Exception {
		
		BaseResponse response = null;
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String uri = propsFile.getStpWsRestUrl() + method;
		String body = mapper.writeValueAsString(requestBody);
		
		HttpEntity<String> entity = new HttpEntity<String>(body, addHeaders());
				
		LOGGER.info("Enviando peticion a la url: " + uri);
		LOGGER.debug("Body Request: " + entity.getBody());
		
		ResponseEntity<BaseResponse> result = restTemplate.exchange(uri, HttpMethod.PUT, entity, BaseResponse.class);
		response = result.getBody();
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + result.getBody());
		
		return response;
	}
	
}
