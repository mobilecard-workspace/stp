/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.stp.consumer.domain.LcpfEstablecimiento;
import com.addcel.stp.consumer.domain.TUsuarios;
import com.addcel.stp.consumer.repository.LcpfEstableciemientoRepository;
import com.addcel.stp.consumer.repository.TUsuariosRepository;
import com.addcel.stp.consumer.response.UsuarioInfo;

@Component
public class ValidarUsuarios {

	private static final Logger LOGGER = LogManager.getLogger(ValidarUsuarios.class);

	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	@Autowired
	private LcpfEstableciemientoRepository lcpfEstableciemientoRepo;
	
	public UsuarioInfo getUserInfo(Long idUsuario, Long idEstablecimiento) {
		UsuarioInfo usuarioInfo = new UsuarioInfo();
		
		if(idUsuario != null) {
			LOGGER.info("Buscando en la BD al usuario: " + idUsuario);
			TUsuarios usuario = tUsuariosRepo.findOne(idUsuario);
			
			if(usuario != null) {
				LOGGER.debug("Usuario encontrado: " + usuario.toString());
				usuarioInfo.setStatus(usuario.getIdUsrStatus());
				String fullname = usuario.getUsrNombre() + " " + usuario.getUsrApellido() + " " + usuario.getUsrMaterno();
				usuarioInfo.setFullname(fullname);
			} else {
				LOGGER.warn("Usuario no encontrado");
				usuarioInfo.setStatus(0);
			}
		} else if(idEstablecimiento != null) {
			LOGGER.info("Buscando en la BD al negocio: " + idEstablecimiento);
			LcpfEstablecimiento establecimiento = lcpfEstableciemientoRepo.findOne(idEstablecimiento);
			
			if(establecimiento != null) {
				LOGGER.debug("Establecimiento encontrado: " + establecimiento.toString());
				usuarioInfo.setStatus(establecimiento.getEstatus());
				usuarioInfo.setFullname(establecimiento.getRazonSocial());
			} else {
				LOGGER.warn("Establecimiento no encontrado");
				usuarioInfo.setStatus(0);
			}
		} else {
			LOGGER.warn("No se envio ningun id para usuario ni para establecimiento");
		}
		
		LOGGER.debug("Usuario Info construido: " + usuarioInfo.toString());		
		return usuarioInfo;
	}
	
}
