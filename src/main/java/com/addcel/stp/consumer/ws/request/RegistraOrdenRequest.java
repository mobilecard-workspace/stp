/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.ws.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistraOrdenRequest {
	
	private String claveRastreo;
	private String conceptoPago;
	private String cuentaBeneficiario;
	private String empresa;
	private String firma;
	private String folioOrigen;
	private String institucionContraparte;
	private String institucionOperante;
	private String monto;
	private String nombreBeneficiario;
	private String nombreOrdenante;
	private String referenciaNumerica;
	private String rfcCurpBeneficiario;
	private String tipoCuentaBeneficiario;
	private String tipoPago;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private String cuentaOrdenante;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private String fechaOperacion;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private String rfcCurpOrdenante;
	
	@JsonInclude(value = Include.NON_EMPTY)
	private String tipoCuentaOrdenante;
	
}
