/**
 * @author Victor Ramirez
 */

package com.addcel.stp.consumer.ws.response;

import lombok.Data;

@Data
public class BaseResponse {
	
	private ResultadoResponse resultado;
	
}
